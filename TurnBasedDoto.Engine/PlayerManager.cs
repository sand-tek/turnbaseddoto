﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TurnBasedDoto.Engine
{
    public class PlayerManager
    {

        #region ---------- Fields ----------

        private Player [] players;
        private int numberOfPlayers;

        #endregion

        #region ---------- Constructors ----------

        public PlayerManager(int numberOfPlayers)
        {
            this.numberOfPlayers = numberOfPlayers;

            players = new Player[numberOfPlayers]; 
        }

        #endregion

        #region ---------- Methods ----------

        /// <summary>
        /// Creates two new player objects and adds them the players array. 
        /// </summary>
        public void Init()
        {
            // Could use numberOfPlayers to loop through and create an 
            // arbitrary number of players. Not sure about generating unique
            // player attribute values in this scenario... 

            players[0] = new Player(1000, 0, new Damage(10, 50, 100));
            players[1] = new Player(1000, 5, new Damage(10, 10, 500));
        }

        /// <summary>
        /// Applies the attacking player's damage to the targetPlayer. 
        /// Returns zero, unless the targetPlayer's HitPoints drop below zero, 
        /// in which case it returns the attackingPlayer's number.
        /// </summary>
        /// <returns></returns>
        public int RightClickAttack(int attackingPlayer, int targetPlayer)
        {
            // How much damage is the attacking player going to deal?
            players[attackingPlayer].Damage.CalculateDamage();

            // Apply that damage to the target player 
            players[targetPlayer].TakeDamage(players[attackingPlayer].Damage.CurrentDamage);

            // Check if he's still alive
            if (players[targetPlayer].HitPoints <= 0)
            {
                return attackingPlayer;
            }

            return 0;
        }

        /// <summary>
        /// Returns a dictionary representing the specified player's values.
        /// </summary>
        /// <param name="playerNumber"></param>
        /// <returns></returns>
        public Dictionary<string, int> PlayerState(int playerNumber)
        {
            Dictionary<string, int> stats = new Dictionary<string, int>();

            stats.Add("HitPoints", players[playerNumber].HitPoints);
            stats.Add("BaseDamage", players[playerNumber].Damage.BaseDamage);
            stats.Add("CritChance", players[playerNumber].Damage.CritChance);
            stats.Add("CritBonus", players[playerNumber].Damage.CritBonus);

            // Maybe Dictionary not the best route, given its homogenity, we can 
            // work around by putting the CriticalHit bool in as 1/0:True/False. 
            // Bit shit though...
            stats.Add("CriticalHit", players[playerNumber].Damage.CriticalHit ? 1 : 0);

            stats.Add("CurrentDamage", players[playerNumber].Damage.CurrentDamage);           
            stats.Add("Armour", players[playerNumber].Armour);

            return stats;
        }

        #endregion
    }
}
