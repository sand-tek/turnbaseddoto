﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TurnBasedDoto.Engine
{
    public class Player
    {
        #region ---------- Fields ----------

        public int HitPoints { get; set; }
        public int Armour { get; set; }
        public bool IsDead { get; private set; }
        public Damage Damage { get; private set; }

        #endregion

        #region ---------- Constructors ----------

        public Player(int hitPoints, int armour, Damage damage)
        {
            HitPoints = hitPoints;         
            Damage = damage;
            Armour = armour;
            IsDead = false;
        }

        #endregion

        #region ---------- Methods ----------

        /// <summary>
        /// Subtracts the incomingDamage to the player's hitpoints, after taking
        /// into account any armour or damage resistance. 
        /// </summary>
        /// <param name="incomingDamage"></param>
        public void TakeDamage(int incomingDamage)
        {
            incomingDamage -= Armour;

            HitPoints -= incomingDamage;

            if (HitPoints <= 0)
            {
                HitPoints = 0;
                IsDead = true;
            }
        }

        public void Attack(Player targetPlayer)
        {
            targetPlayer.TakeDamage(Damage.CurrentDamage);
        }

        public override string ToString()
        {
            return "HitPoints: " + HitPoints +
                "\nBase Damage: " + Damage.BaseDamage +
                "\nCrit Chance: " + Damage.CritChance + 
                "\nCrit Bonus: " + Damage.CritBonus +
                "\nArmour: " + Armour;
        }

        #endregion
    }
}
