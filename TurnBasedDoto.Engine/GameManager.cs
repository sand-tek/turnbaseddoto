﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TurnBasedDoto.Engine
{
    public class GameManager
    {

        #region ---------- Fields ----------

        private Player [] players;

        #endregion

        #region ---------- Constructors ----------

        public GameManager()
        {
            players = new Player[2]; 
        }

        #endregion

        #region ---------- Methods ----------

        /// <summary>
        /// Creates two new player objects and adds them the players array. 
        /// </summary>
        public void Init()
        {
            // Could use numberOfPlayers to loop through and create an 
            // arbitrary number of players. Not sure about generating unique
            // player attribute values in this scenario... 

            players[0] = new Player(1000, 0, new Damage(10, 50, 100));
            players[1] = new Player(1000, 5, new Damage(10, 10, 500));
        }

        public Player [] GetPlayers()
        {
            return players;
        }

        #endregion
    }
}
