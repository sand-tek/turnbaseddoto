﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TurnBasedDoto.Engine
{
    public class Damage
    {

        #region ---------- Fields ----------

        public bool CriticalHit { get; private set; }
        public int CurrentDamage { get; private set; }
        public int CritChance { get; private set; }
        public int BaseDamage { get; private set; }
        public int CritBonus { get; private set; }

        #endregion

        #region ---------- Constructors ----------

        public Damage(int baseDamage, int critChance, int critBonus)
        {
            BaseDamage = baseDamage;

            CritChance = critChance;
            CritBonus = critBonus;

            CriticalHit = false;
        }

        #endregion

        #region ---------- Methods ----------

        /// <summary>
        /// Calculates the chance of a critical strike, sets CriticalStrike 
        /// and CurrentDamage properties accordingly.
        /// </summary>
        public void CalculateDamage()
        {
            Random random = new Random();
            int randomNumber = random.Next(1, 100);

            if (randomNumber < CritChance)
            {
                CriticalHit = true;
                CurrentDamage = BaseDamage + CritBonus;
            }
            else
            {
                CriticalHit = false;
                CurrentDamage = BaseDamage;
            }
        }

        #endregion
    }
}
