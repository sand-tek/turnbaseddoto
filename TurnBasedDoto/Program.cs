﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using TurnBasedDoto.Engine;

namespace TurnBasedDoto
{
    class Program
    {
        static GameManager game;
        static Player playerOne;
        static Player playerTwo;

        static void Main(string[] args)
        {
            game = new GameManager();

            game.Init();

            playerOne = game.GetPlayers()[0];
            playerTwo = game.GetPlayers()[1];

            Console.WriteLine("WELCOME TO TURN BASED DOTO!");
            Console.WriteLine();

            Console.WriteLine("Player 1's starting attributes: ");
            Console.WriteLine(playerOne.ToString());
            Console.WriteLine();

            Console.WriteLine("Player 2's starting attributes: ");
            Console.WriteLine(playerTwo.ToString());
            Console.WriteLine();

            Console.WriteLine("Press enter to start!");
            Console.ReadLine();

            int winningPlayer = 0;

            while (winningPlayer == 0)
            {
                playerOne.Damage.CalculateDamage();
                playerOne.Attack(playerTwo);

                if (playerTwo.IsDead)
                    winningPlayer = 1;

                PrintAttackResult(playerOne, playerTwo);

                playerTwo.Damage.CalculateDamage();
                playerTwo.Attack(playerOne);

                if (playerOne.IsDead)
                    winningPlayer = 2;

                PrintAttackResult(playerTwo, playerOne);

                Console.ReadLine();
            }

            Console.WriteLine("Player {0} wins!", winningPlayer);
            Console.ReadLine();
        }

        #region ---------- Print Statements ----------

        public static void PrintAttackResult(Player attackingPlayer, Player targetPlayer)
        {

            if (attackingPlayer.Damage.CriticalHit)
            {
                Console.WriteLine("Player scores a Critical Hit!");
            }

            Console.WriteLine("Attacking player does {0} damage.", 
                attackingPlayer.Damage.CurrentDamage);

            Console.WriteLine("Target player has {0} hitpoints.",
                targetPlayer.HitPoints);

            Console.WriteLine();
        }

        #endregion

    }
}
